package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface IAccountGroupDataAccess
        extends IReferenceParentEntityDataAccess<AccountGroup> {
}
