package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ICurrencyRateDataAccess
        extends  IEntityDataAccess<CurrencyRate> {
}
