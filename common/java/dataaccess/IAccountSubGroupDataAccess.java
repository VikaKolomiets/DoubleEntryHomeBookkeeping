package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface IAccountSubGroupDataAccess
        extends IReferenceChildEntityDataAccess<AccountSubGroup>, IParentDataAccess<AccountSubGroup>
{
}
