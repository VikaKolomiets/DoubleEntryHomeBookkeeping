package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ITemplateGroupDataAccess
        extends IReferenceParentEntityDataAccess<TemplateGroup>{
}
