package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ICategoryGroupDataAccess
        extends IReferenceParentEntityDataAccess<CategoryGroup> {
}
