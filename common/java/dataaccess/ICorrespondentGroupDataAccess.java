package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ICorrespondentGroupDataAccess
        extends IReferenceParentEntityDataAccess<CorrespondentGroup> {
}
