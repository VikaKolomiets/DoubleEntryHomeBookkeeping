package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ICategoryDataAccess
        extends IReferenceChildEntityDataAccess<Category> {
}
