package common.dataaccess.base;

import common.models.interfaces.*;

public interface IParentDataAccess<T extends IEntity>  {
    void loadChildren(T entity);
}
