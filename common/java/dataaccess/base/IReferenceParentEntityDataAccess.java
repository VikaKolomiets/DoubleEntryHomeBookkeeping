package common.dataaccess.base;

import common.models.interfaces.*;

public interface IReferenceParentEntityDataAccess <T extends IEntity & INamedEntity>
        extends IEntityDataAccess<T>, IParentEntityDataAccess<T>{
}
