package common.dataaccess.base;

import common.models.interfaces.*;

import java.util.*;

public interface IParentEntityDataAccess<T extends IEntity & INamedEntity>
        extends IParentDataAccess<T> {
    ArrayList<T> getByName(String name);
    int getMaxOrder();
    int getCount();
    ArrayList<T> getList();
}
