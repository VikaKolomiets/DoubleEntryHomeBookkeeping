package common.dataaccess.base;

import common.models.interfaces.*;

public interface IChildDataAccess<T extends IEntity>  {
    void loadParent(T entity);
}
