package common.dataaccess.base;

import common.models.interfaces.*;

public interface IReferenceChildEntityDataAccess<T extends IEntity & INamedEntity>
        extends IEntityDataAccess<T>, IChildEntityDataAccess<T>  {
}
