package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

import java.util.ArrayList;

public interface IAccountDataAccess
        extends IReferenceChildEntityDataAccess<Account> {
    void loadCurrency(Account account);
    ArrayList<Account> getAccountsByCorrespondent(Correspondent correspondent);
    ArrayList<Account> getAccountsByCategory(Category category);
    ArrayList<Account> getAccountsByProject(Project project);
}

