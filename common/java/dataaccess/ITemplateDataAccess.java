package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

import java.util.*;

public interface ITemplateDataAccess
        extends IReferenceChildEntityDataAccess<Template>{
    ArrayList<TemplateEntry> getEntriesByAccount(Account account);
    int getTemplateEntriesCount(UUID accountId);
}
