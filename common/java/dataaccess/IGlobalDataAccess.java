package common.dataaccess;

import common.models.interfaces.*;

import java.util.*;

public interface IGlobalDataAccess {
    void save();
    IEntity getEntity(UUID id);
}
