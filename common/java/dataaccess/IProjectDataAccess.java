package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface IProjectDataAccess
        extends IReferenceChildEntityDataAccess<Project> {
}
