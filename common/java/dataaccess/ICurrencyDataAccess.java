package common.dataaccess;

import common.dataaccess.base.*;
import common.models.Currency;

import java.util.*;

public interface ICurrencyDataAccess
        extends IEntityDataAccess<Currency> {

    ArrayList<Currency> getByIsoCode(String isoCode);
    int getMaxOrder();
    int getCount();
    ArrayList<Currency> getList();
}
