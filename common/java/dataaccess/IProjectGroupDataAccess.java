package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface IProjectGroupDataAccess
        extends IReferenceParentEntityDataAccess<ProjectGroup> {
}
