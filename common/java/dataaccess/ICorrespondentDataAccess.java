package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

public interface ICorrespondentDataAccess
        extends IReferenceChildEntityDataAccess<Correspondent> {
}
