package common.dataaccess;

import common.dataaccess.base.*;
import common.models.*;

import java.util.*;

public interface ITransactionDataAccess
        extends IEntityDataAccess<Transaction> {
    ArrayList<TransactionEntry> getEntriesByAccount(Account account);
    int getTransactionEntriesCount(UUID accountId);
}
