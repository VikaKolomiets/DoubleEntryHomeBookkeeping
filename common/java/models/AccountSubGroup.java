package common.models;

import common.models.base.*;
import common.models.interfaces.*;

public class AccountSubGroup extends ReferenceParentEntity<Account> implements IReferenceChildEntity<AccountGroup> {

    private AccountGroup parent;

    public AccountGroup getParent() {
        return this.parent;
    }
    public void setParent(AccountGroup value) {
        this.parent = value;
    }
}
