package common.models.base;

import common.models.interfaces.*;
import java.util.*;

public class ReferenceParentEntity <T> extends ReferenceEntity implements IReferenceParentEntity<T> {

    private final ArrayList<T> children = new ArrayList<>();

    public ArrayList<T> getChildren() {
        return this.children;
    }

}
