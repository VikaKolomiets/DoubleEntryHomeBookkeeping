package common.models.interfaces;

public interface IReferenceChildEntity<T> extends IReferenceEntity, IChildEntity<T> {
}
