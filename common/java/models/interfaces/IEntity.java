package common.models.interfaces;

import java.util.*;

public interface IEntity {
    UUID getId();
    void setId(UUID id);
}
