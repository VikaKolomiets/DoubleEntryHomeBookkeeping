package common.models.interfaces;

public interface ITrackedEntity {
    String getTimeStamp();
    void setTimeStamp(String value);
}
