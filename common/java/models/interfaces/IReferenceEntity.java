package common.models.interfaces;

public interface IReferenceEntity extends  IEntity, ITrackedEntity, INamedEntity, IOrderedEntity, IFavoriteEntity {
}
