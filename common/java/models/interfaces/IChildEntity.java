package common.models.interfaces;

public interface IChildEntity<T> {
    T getParent();
    void setParent(T value);
}
