package common.models.interfaces;

public interface IReferenceParentEntity<T> extends IReferenceEntity, IParentEntity<T> {
}
