package common.models.interfaces;

public interface INamedEntity {
    String getName();
    void setName(String value);

    String getDescription();
    void setDescription(String value);
}
