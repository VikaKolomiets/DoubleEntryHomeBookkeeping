package common.models.interfaces;

public interface IOrderedEntity {
    int getOrder();
    void setOrder(int value);
}
