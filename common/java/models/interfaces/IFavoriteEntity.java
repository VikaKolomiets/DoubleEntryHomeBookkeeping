package common.models.interfaces;

public interface IFavoriteEntity {
    boolean getIsFavorite();
    void setIsFavorite(boolean value);
}
