package common.models.interfaces;

import java.util.*;

public interface IParentEntity<T> {
    ArrayList<T> getChildren();
}
