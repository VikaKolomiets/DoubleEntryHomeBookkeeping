package common.models.enums;

public enum TransactionState {
    NoValid,
    Draft,
    Planned,
    Confgirmed
}
