package common.services;

import common.services.base.*;

public interface ICurrencyService extends IOrderedEntityService, IFavoriteEntityService{
    void add(String isoCode);
    void delete(String isoCode);
}
