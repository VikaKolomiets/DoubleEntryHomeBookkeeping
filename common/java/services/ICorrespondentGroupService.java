package common.services;

import common.models.*;
import common.services.base.*;

public interface ICorrespondentGroupService extends IReferenceEntityService<CorrespondentGroup>{
}
