package common.services;

import common.models.*;
import common.services.base.*;

public interface ICategoryService extends IReferenceChildEntityService<Category>, ICombinedEntityService{
}
