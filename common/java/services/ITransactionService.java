package common.services;

import common.models.*;
import common.services.base.*;

import java.util.ArrayList;
import java.util.UUID;

public interface ITransactionService extends IEntityService<Transaction> {
    void deleteTransactionList(ArrayList<UUID> transactionIds);
}
