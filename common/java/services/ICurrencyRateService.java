package common.services;

import common.models.*;
import common.services.base.*;

public interface ICurrencyRateService extends IEntityService<CurrencyRate>{
}
