package common.services;

import common.models.*;
import common.services.base.*;

public interface IAccountService extends IReferenceChildEntityService<Account>, ICombinedEntityService {
}
