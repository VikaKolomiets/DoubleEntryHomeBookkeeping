package common.services;

import common.models.*;
import common.services.base.*;

public interface IAccountSubGroupService extends IReferenceChildEntityService<AccountSubGroup> {
}
