package common.services;

import common.models.*;
import common.services.base.*;

public interface ICorrespondentService extends IReferenceChildEntityService<Correspondent>, ICombinedEntityService{
}
