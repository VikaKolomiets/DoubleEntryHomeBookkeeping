package common.services.base;

import common.models.interfaces.IEntity;

public interface IReferenceChildEntityService<T extends IEntity> extends IReferenceEntityService<T>, IChildEntityService  {
}
