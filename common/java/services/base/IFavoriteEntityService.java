package common.services.base;

import java.util.UUID;

public interface IFavoriteEntityService {
    void setFavoriteStatus(UUID entityId, boolean isFavorite);
}
