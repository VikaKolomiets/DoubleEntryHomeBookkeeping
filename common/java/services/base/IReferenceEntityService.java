package common.services.base;

import common.models.interfaces.IEntity;

public interface IReferenceEntityService<T extends IEntity> extends IEntityService<T>, IOrderedEntityService, IFavoriteEntityService {
}
