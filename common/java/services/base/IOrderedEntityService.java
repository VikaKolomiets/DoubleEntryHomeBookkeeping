package common.services.base;

import java.util.UUID;

public interface IOrderedEntityService {
    void setOrder(UUID entityId, int order);
}
