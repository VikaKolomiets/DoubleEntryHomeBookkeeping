package common.services.base;

import common.models.interfaces.IEntity;

import java.util.UUID;

public interface IEntityService<T extends IEntity> {
    void add(T entity);
    void update(T entity);
    void delete(UUID entityId);
}
