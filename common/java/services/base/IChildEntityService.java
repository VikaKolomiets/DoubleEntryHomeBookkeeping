package common.services.base;

import java.util.UUID;

public interface IChildEntityService {
    void moveToAnotherParent(UUID entityId, UUID parentId);
}
