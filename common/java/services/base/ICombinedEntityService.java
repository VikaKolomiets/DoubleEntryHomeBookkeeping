package common.services.base;

import java.util.UUID;

public interface ICombinedEntityService {
    void combineTwoEntities(UUID primaryId, UUID secondaryId);
}
