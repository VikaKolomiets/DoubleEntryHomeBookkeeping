package common.services;

import common.models.*;
import common.services.base.*;

public interface IAccountGroupService extends IReferenceEntityService<AccountGroup> {
}
