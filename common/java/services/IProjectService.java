package common.services;

import common.models.*;
import common.services.base.*;

public interface IProjectService extends IReferenceChildEntityService<Project>, ICombinedEntityService{
}
