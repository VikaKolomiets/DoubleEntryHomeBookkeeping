package dataaccess.inmemorydb.xml.persistence;

import org.w3c.dom.Document;

public interface IXmlValidator {
    void Validate(Document doc);
}
