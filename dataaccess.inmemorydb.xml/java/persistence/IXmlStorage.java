package dataaccess.inmemorydb.xml.persistence;

import org.w3c.dom.Document;

public interface IXmlStorage {
    Document load();
    void save(Document doc);

}
