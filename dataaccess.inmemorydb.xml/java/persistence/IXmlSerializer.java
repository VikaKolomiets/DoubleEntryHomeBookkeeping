package dataaccess.inmemorydb.xml.persistence;

import dataaccess.inmemorydb.Ledger;
import org.w3c.dom.Document;

public interface IXmlSerializer {
    Document serialize(Ledger ledger);
    Ledger deserialize(Document document);

}
