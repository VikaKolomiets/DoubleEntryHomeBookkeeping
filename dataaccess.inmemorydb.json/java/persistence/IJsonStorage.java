package dataaccess.inmemorydb.json.persistence;

import org.w3c.dom.Document;

public interface IJsonStorage {
    Document load();
    void save(Document doc);

}
