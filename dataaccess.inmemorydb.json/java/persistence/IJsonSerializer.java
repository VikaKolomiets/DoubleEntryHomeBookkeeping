package dataaccess.inmemorydb.json.persistence;

import dataaccess.inmemorydb.Ledger;
import org.w3c.dom.Document;

public interface IJsonSerializer {
    Document serialize(Ledger ledger);
    Ledger deserialize(Document document);

}
