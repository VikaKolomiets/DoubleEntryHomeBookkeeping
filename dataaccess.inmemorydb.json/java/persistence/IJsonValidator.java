package dataaccess.inmemorydb.json.persistence;

import org.w3c.dom.Document;

public interface IJsonValidator {
    void Validate(Document doc);
}
