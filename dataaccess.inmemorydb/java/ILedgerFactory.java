package dataaccess.inmemorydb;

public interface ILedgerFactory {
    Ledger get();
    void send();
}
