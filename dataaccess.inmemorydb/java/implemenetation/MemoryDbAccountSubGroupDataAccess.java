package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbAccountSubGroupDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<AccountSubGroup, AccountGroup>
        implements IAccountSubGroupDataAccess {
    public MemoryDbAccountSubGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    public void loadChildren(AccountSubGroup entity) {
        //Only for Relational Db
    }

    protected ArrayList<AccountSubGroup> getEntities() {
        return this.getLedger().getAccountSubGroups();
    }
}
