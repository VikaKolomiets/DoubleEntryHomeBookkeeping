package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbAccountGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<AccountGroup>
        implements IAccountGroupDataAccess {

    public MemoryDbAccountGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<AccountGroup> getEntities() {
         return this.getLedger().getAccountGroups();
    }
}
