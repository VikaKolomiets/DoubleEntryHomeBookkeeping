package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCorrespondentGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<CorrespondentGroup>
        implements ICorrespondentGroupDataAccess {
    public MemoryDbCorrespondentGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<CorrespondentGroup> getEntities() {
        return this.getLedger().getCorrespondentGroups();
    }
}
