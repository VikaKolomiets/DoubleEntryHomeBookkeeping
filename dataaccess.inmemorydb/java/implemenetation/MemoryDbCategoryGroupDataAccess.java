package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCategoryGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<CategoryGroup>
        implements ICategoryGroupDataAccess {

    public MemoryDbCategoryGroupDataAccess(ILedgerFactory ledgerFactory, ILedgerFactory factory) {
        super(ledgerFactory);
    }

    protected ArrayList<CategoryGroup> getEntities() {
        return this.getLedger().getCategoryGroups();
    }
}
