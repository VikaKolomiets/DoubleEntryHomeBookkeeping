package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbTemplateGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<TemplateGroup>
        implements ITemplateGroupDataAccess {
    public MemoryDbTemplateGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<TemplateGroup> getEntities() {
        return this.getLedger().getTemplateGroups();
    }
}
