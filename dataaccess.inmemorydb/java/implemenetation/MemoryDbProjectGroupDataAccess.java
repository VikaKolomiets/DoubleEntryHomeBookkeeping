package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbProjectGroupDataAccess
        extends MemoryDbReferenceParentEntityDataAccess<ProjectGroup>
        implements IProjectGroupDataAccess {
    public MemoryDbProjectGroupDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<ProjectGroup> getEntities()  {
        return this.getLedger().getProjectGroups();
    }
}
