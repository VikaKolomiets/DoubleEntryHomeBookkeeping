package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import dataaccess.inmemorydb.*;

import java.time.*;

public class MemoryDbSystemConfigDataAccess
        implements ISystemConfigDataAccess {
    private final ILedgerFactory factory;

    public MemoryDbSystemConfigDataAccess(ILedgerFactory factory) {
        this.factory = factory;
    }

    public String getMainCurrencyIsoCode() {
        return factory.get().getSystemConfig().getMainCurrencyIsoCode();
    }

    public LocalDateTime getMinDate() {
        return factory.get().getSystemConfig().getMinDate();
    }

    public LocalDateTime getMaxDate() {
        return factory.get().getSystemConfig().getMaxDate();
    }
}
