package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbProjectDataAccess extends MemoryDbReferenceChildEntityDataAccess<Project, ProjectGroup> implements IProjectDataAccess{
    public MemoryDbProjectDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Project> getEntities() {
        return this.getLedger().getProjects();
    }
}
