package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCurrencyRateDataAccess
        extends MemoryDbEntityDataAccess<CurrencyRate>
        implements ICurrencyRateDataAccess {
    public MemoryDbCurrencyRateDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<CurrencyRate> getEntities() {
        return this.getLedger().getCurrencyRates();
    }
}
