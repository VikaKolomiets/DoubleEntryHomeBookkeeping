package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCorrespondentDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<Correspondent, CorrespondentGroup>
        implements ICorrespondentDataAccess {
    public MemoryDbCorrespondentDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Correspondent> getEntities() {
        return this.getLedger().getCorrespondents();
    }
}
