package dataaccess.inmemorydb.implemenetation;

import common.dataaccess.*;
import common.models.*;
import dataaccess.inmemorydb.*;
import dataaccess.inmemorydb.implemenetation.base.*;

import java.util.*;

public class MemoryDbCategoryDataAccess
        extends MemoryDbReferenceChildEntityDataAccess<Category, CategoryGroup>
        implements ICategoryDataAccess {
    public MemoryDbCategoryDataAccess(ILedgerFactory ledgerFactory) {
        super(ledgerFactory);
    }

    protected ArrayList<Category> getEntities() {
        return this.getLedger().getCategories();
    }
}
