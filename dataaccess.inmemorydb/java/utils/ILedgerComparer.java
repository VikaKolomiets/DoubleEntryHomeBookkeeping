package dataaccess.inmemorydb.utils;

import dataaccess.inmemorydb.Ledger;

public interface ILedgerComparer {
    boolean IsEquals(Ledger first, Ledger second);
}
