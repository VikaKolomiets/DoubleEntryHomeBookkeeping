package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

public class AccountGroupService extends ReferenceParentEntityService<AccountGroup, AccountSubGroup> implements IAccountGroupService {
    public AccountGroupService(
            IGlobalDataAccess globalDataAccess,
            IAccountGroupDataAccess entityDataAccess,
            IAccountSubGroupDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }
}
