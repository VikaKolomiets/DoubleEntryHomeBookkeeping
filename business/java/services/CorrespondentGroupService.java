package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

public class CorrespondentGroupService extends ReferenceParentEntityService<CorrespondentGroup, Correspondent> implements ICorrespondentGroupService {
    public CorrespondentGroupService(
            IGlobalDataAccess globalDataAccess,
            ICorrespondentGroupDataAccess entityDataAccess,
            ICorrespondentDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }
}
