package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

import java.util.*;

public class CategoryService extends ReferenceChildEntityService<Category, CategoryGroup> implements ICategoryService {
    public CategoryService(
            IGlobalDataAccess globalDataAccess,
            ICategoryDataAccess entityDataAccess,
            ICategoryGroupDataAccess parentEntityDataAccess,
            IAccountDataAccess accountDataAccess) {
        super(globalDataAccess, entityDataAccess, parentEntityDataAccess, accountDataAccess);
    }

    
    protected ArrayList<Account> GetAccountsByEntity(Category entity) {
        return this.getAccountDataAccess().getAccountsByCategory(entity);
    }
    
    protected void AccountEntitySetter(Category entity, Account account) {
        account.setCategory(entity);
    }
}
