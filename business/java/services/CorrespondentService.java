package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

import java.util.*;

public class CorrespondentService extends ReferenceChildEntityService<Correspondent, CorrespondentGroup> implements ICorrespondentService {
    public CorrespondentService(
            IGlobalDataAccess globalDataAccess,
            ICorrespondentDataAccess entityDataAccess,
            ICorrespondentGroupDataAccess parentEntityDataAccess,
            IAccountDataAccess accountDataAccess) {
        super(globalDataAccess, entityDataAccess, parentEntityDataAccess, accountDataAccess);
    }

    
    protected ArrayList<Account> GetAccountsByEntity(Correspondent entity) {
        return this.getAccountDataAccess().getAccountsByCorrespondent(entity);
    }
    
    protected void AccountEntitySetter(Correspondent entity, Account account) {
        account.setCorrespondent(entity);
    }
}
