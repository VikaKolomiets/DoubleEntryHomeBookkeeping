package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

public class CategoryGroupService extends ReferenceParentEntityService<CategoryGroup, Category> implements ICategoryGroupService {
    public CategoryGroupService(
            IGlobalDataAccess globalDataAccess,
            ICategoryGroupDataAccess entityDataAccess,
            ICategoryDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }
}
