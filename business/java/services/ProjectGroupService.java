package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

public class ProjectGroupService extends ReferenceParentEntityService<ProjectGroup, Project> implements IProjectGroupService {
    public ProjectGroupService(
            IGlobalDataAccess globalDataAccess,
            IProjectGroupDataAccess entityDataAccess,
            IProjectDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }
}
