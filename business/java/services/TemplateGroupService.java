package business.services;

import business.services.base.*;
import common.dataaccess.*;
import common.models.*;
import common.services.*;

public class TemplateGroupService extends ReferenceParentEntityService<TemplateGroup, Template> implements ITemplateGroupService {
    public TemplateGroupService(
            IGlobalDataAccess globalDataAccess,
            ITemplateGroupDataAccess entityDataAccess,
            ITemplateDataAccess childEntityDataAccess) {
        super(globalDataAccess, entityDataAccess, childEntityDataAccess);
    }
}
