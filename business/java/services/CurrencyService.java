package business.services;

import business.utils.*;
import common.dataaccess.*;
import common.services.*;

import java.util.*;

public class CurrencyService  implements ICurrencyService {
    private final IGlobalDataAccess globalDataAccess;
    private final ICurrencyDataAccess currencyDataAccess;

    public CurrencyService(IGlobalDataAccess globalDataAccess, ICurrencyDataAccess currencyDataAccess) {
        this.globalDataAccess = globalDataAccess;
        this.currencyDataAccess = currencyDataAccess;
    }

    public void add(String isoCode) {
        Guard.checkObjectForNull(isoCode, "isoCode");
    }

    public void delete(String isoCode) {
        Guard.checkObjectForNull(isoCode, "isoCode");
    }

    public void setFavoriteStatus(UUID entityId, boolean isFavorite) {
        Guard.checkObjectForNull(entityId, "entityId");
    }

    public void setOrder(UUID entityId, int order) {
        Guard.checkObjectForNull(entityId, "entityId");
    }

}
